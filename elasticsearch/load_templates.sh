#!/bin/bash

echo -e "\x1B[7mHost\x1B[27m"
read host

echo -e "\x1B[7mPort\x1B[27m"
read port

echo -e "\x1B[7mUser\x1B[27m"
read user

echo -e "\x1B[7mPassword\x1B[27m"
read -s password

bash ./zpa-connector-status_template.sh $host $port $user $password
bash ./zpa-user-status_template.sh $host $port $user $password
bash ./zpa-user-activity_template.sh $host $port $user $password