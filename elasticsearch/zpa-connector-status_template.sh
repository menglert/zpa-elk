#!/bin/bash

curl -u $3:$4 -X PUT "$1:$2/_template/zpa-connector-status_template?include_type_name=true" -H 'Content-Type: application/json' -d'{
  "version": 1,
  "order": 0,
  "index_patterns": [
    "zpa-connector-status*"
  ],
  "mappings": {
    "_doc": {
      "_source": {
        "excludes": [],
        "includes": [],
        "enabled": true
      },
      "_meta": {},
      "_routing": {
        "required": false
      },
      "dynamic": true,
      "numeric_detection": true,
      "date_detection": true,
      "dynamic_date_formats": [
        "strict_date_optional_time",
        "yyyy/MM/dd HH:mm:ss Z||yyyy/MM/dd Z"
      ],
      "properties": {
        "ConnectorStatus.Location": {
          "type": "geo_point"
        },
        "ConnectorStatus.LogTimestamp": {
          "index": true,
          "ignore_malformed": true,
          "store": false,
          "type": "date",
          "doc_values": true
        },
        "ConnectorStatus.TimestampAuthentication": {
          "index": true,
          "ignore_malformed": true,
          "store": false,
          "type": "date",
          "doc_values": true
        },
        "ConnectorStatus.TimestampUnAuthentication": {
          "index": true,
          "ignore_malformed": true,
          "store": false,
          "type": "date",
          "doc_values": true
        }
      }
    }
  }
}
'