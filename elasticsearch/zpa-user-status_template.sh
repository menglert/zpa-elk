#!/bin/bash

curl -u $3:$4 -X PUT "$1:$2/_template/zpa-user-status_template?include_type_name=true" -H 'Content-Type: application/json' -d'{
    "version": 1,
    "order": 0,
    "index_patterns": [
      "zpa-user-status*"
    ],
    "mappings": {
      "mapping": {
        "_source": {
          "excludes": [],
          "includes": [],
          "enabled": true
        },
        "_meta": {},
        "_routing": {
          "required": false
        },
        "dynamic": true,
        "numeric_detection": true,
        "date_detection": true,
        "dynamic_date_formats": [
          "strict_date_optional_time",
          "yyyy/MM/dd HH:mm:ss Z||yyyy/MM/dd Z"
        ],
        "dynamic_templates": [],
        "properties": {
          "UserStatus.Location": {
            "type": "geo_point"
          },
          "UserStatus.ZENLocation": {
            "type": "geo_point"
          }
        }
      }
    }
  }
'