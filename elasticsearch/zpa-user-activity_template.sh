#!/bin/bash

curl -u $3:$4 -X PUT "$1:$2/_template/zpa-user-activity_template?include_type_name=true" -H 'Content-Type: application/json' -d'{
  "version": 1,
  "order": 0,
  "index_patterns": [
    "zpa-user-activity*"
  ],
  "mappings": {
    "mapping": {
      "_source": {
        "excludes": [],
        "includes": [],
        "enabled": true
      },
      "_meta": {},
      "_routing": {
        "required": false
      },
      "dynamic": true,
      "numeric_detection": true,
      "date_detection": true,
      "dynamic_date_formats": [
        "strict_date_optional_time",
        "yyyy/MM/dd HH:mm:ss Z||yyyy/MM/dd Z"
      ],
      "dynamic_templates": [],
      "properties": {
        "UserActivity.Location": {
          "type": "geo_point"
        },
        "UserActivity.TimestampAppLearnStart": {
          "index": true,
          "ignore_malformed": true,
          "store": false,
          "type": "date",
          "doc_values": true
        },
        "UserActivity.TimestampCARx": {
          "index": true,
          "ignore_malformed": true,
          "store": false,
          "type": "date",
          "doc_values": true
        },
        "UserActivity.TimestampCATx": {
          "index": true,
          "ignore_malformed": true,
          "store": false,
          "type": "date",
          "doc_values": true
        },
        "UserActivity.TimestampConnectionEnd": {
          "index": true,
          "ignore_malformed": true,
          "store": false,
          "type": "date",
          "doc_values": true
        },
        "UserActivity.TimestampConnectionStart": {
          "index": true,
          "ignore_malformed": true,
          "store": false,
          "type": "date",
          "doc_values": true
        },
        "UserActivity.TimestampConnectorZENSetupComplete": {
          "index": true,
          "ignore_malformed": true,
          "store": false,
          "type": "date",
          "doc_values": true
        },
        "UserActivity.TimestampZENFirstRxClient": {
          "index": true,
          "ignore_malformed": true,
          "store": false,
          "type": "date",
          "doc_values": true
        },
        "UserActivity.TimestampZENFirstRxConnector": {
          "index": true,
          "ignore_malformed": true,
          "store": false,
          "type": "date",
          "doc_values": true
        },
        "UserActivity.TimestampZENFirstTxClient": {
          "index": true,
          "ignore_malformed": true,
          "store": false,
          "type": "date",
          "doc_values": true
        },
        "UserActivity.TimestampZENFirstTxConnector": {
          "index": true,
          "ignore_malformed": true,
          "store": false,
          "type": "date",
          "doc_values": true
        },
        "UserActivity.TimestampZENLastRxClient": {
          "index": true,
          "ignore_malformed": true,
          "store": false,
          "type": "date",
          "doc_values": true
        },
        "UserActivity.TimestampZENLastRxConnector": {
          "index": true,
          "ignore_malformed": true,
          "store": false,
          "type": "date",
          "doc_values": true
        },
        "UserActivity.TimestampZENLastTxClient": {
          "index": true,
          "ignore_malformed": true,
          "store": false,
          "type": "date",
          "doc_values": true
        },
        "UserActivity.TimestampZENLastTxConnector": {
          "index": true,
          "ignore_malformed": true,
          "store": false,
          "type": "date",
          "doc_values": true
        }
      }
    }
  }
}
'